const { validationResult } = require('express-validator')

module.exports = (req, res, next) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    const message = errors.array().map((x) => ({ [x.param]: x.msg }))
    return res.status(400).json(message)
  }

  next()
}
