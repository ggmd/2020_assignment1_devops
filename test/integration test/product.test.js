const app = require('../../src/app')
const supertest = require('supertest')
const request = supertest(app)

// create element for insert test
const newProduct = {
  id: 1,
  name: 'Chiavetta usb',
  description: 'Chiavetta usb 3.0 da 32GB',
  price: 18,
  quantity: 144
}

describe('test delete specific element in database', () => {
  it('delete specific element', async () => {
    await request.delete('/api/v1/products/' + String(newProduct.id))
  })
  it('test for control element is deleted', async () => {
    const res = await request.get('/api/v1/products/' + String(newProduct.id))
    expect(res.status).toBe(404)
  })
  it('test delete element not in database', async () => {
    const res = await request.delete('/api/v1/products/' + String(newProduct.id))
    expect(res.status).toBe(404)
  })
})

describe('test insert a new element in database', () => {
  it('insert new element', async () => {
    const res = await request.post('/api/v1/products/').send(newProduct)
    expect(res.status).toBe(201)
  })

  it('test for control insert new element', async () => {
    const res = await request.get('/api/v1/products/' + String(newProduct.id))
    expect(res.status).toBe(200)
    expect(String(res.body.name)).toBe('Chiavetta usb')
    expect(String(res.body.description)).toBe('Chiavetta usb 3.0 da 32GB')
    expect(String(res.body.price)).toBe('18')
    expect(String(res.body.quantity)).toBe('144')
  })
})

it('test get all data in database', async () => {
  const res = await request.get('/api/v1/products/')
  expect(res.status).toBe(200)
})

it('test get specific data in database', async () => {
  const res = await request.get('/api/v1/products/' + String(newProduct.id))
  expect(res.status).toBe(200)
  expect(String(res.body.name)).toBe('Chiavetta usb')
  expect(String(res.body.description)).toBe('Chiavetta usb 3.0 da 32GB')
  expect(String(res.body.price)).toBe('18')
  expect(String(res.body.quantity)).toBe('144')
})
