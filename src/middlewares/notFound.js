module.exports = (req, res) => {
  res.status(404).json({
    status: 404,
    message: `Route ${req.url} not found.`
  })
}
