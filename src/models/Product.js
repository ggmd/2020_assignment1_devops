module.exports = (sequelize, Sequelize) => {
  const Product = sequelize.define('product', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      validate: {
        isInt: true
      }
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please enter a name for your item'
        },
        notEmpty: {
          msg: 'Please enter a not empty name for your item'
        }
      }
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please enter your description'
        },
        notEmpty: {
          msg: 'Please enter a not empty description'
        }
      }
    },
    price: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      validate: {
        min: 0,
        notNull: {
          msg: 'Please enter your price'
        }
      }
    },
    quantity: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        min: 0,
        notNull: {
          msg: 'Please enter your quantity'
        }
      }
    }
  },
  {
    createdAt: false,
    updatedAt: false
  })

  return Product
}
