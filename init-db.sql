CREATE DATABASE mydatabase;

DROP TABLE products;
CREATE TABLE products 
    (id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    price DECIMAL NOT NULL,
    quantity INTEGER NOT NULL);

INSERT INTO products (id,name,description,price,quantity) VALUES (1,'GEFORCE RTX 3080','GeForce RTX 3080 offre le prestazioni strabilianti che tutti i giocatori vorrebbero, grazie ad Ampere: architettura RTX NVIDIA di 2a generazione. È costruita con core RT e Tensor Core potenziati, nuovi multiprocessori di streaming e una memoria G6X superveloce, il tutto per offrirti una straordinaria esperienza di gioco.',719.00,100);
INSERT INTO products (id,name,description,price,quantity) VALUES (2,'Echo Dot (3ª generazione)','Altoparlante intelligente con integrazione Alexa - Tessuto antracite',49.99,80);
INSERT INTO products (id,name,description,price,quantity) VALUES (3,'Apple MacBook Pro','(16", 16GB RAM, Archiviazione 1TB) - Grigio siderale',2980.00,135);
INSERT INTO products (id,name,description,price,quantity) VALUES (4,'Xiaomi Mi Monopattino Elettrico Pro 2','Versione Amazon con Lucchetto Incluso, 45 Km di Autonomia, Velocità Fino a 25 Km/h',529.90,30);
INSERT INTO products (id,name,description,price,quantity) VALUES (5,'Samsung Galaxy Note20','Ultra 5G Smartphone, Display 6.9" Dynamic Amoled 2X, 3 Fotocamere, 256Gb Espandibili, Ram 12Gb, Batteria 4500Mah, Hybrid Sim+Esim, Android 10, Mystic Black',1149.00, 18);
INSERT INTO products (id,name,description,price,quantity) VALUES (6,'Sony PlayStation 5','Console Fissa Sony PlayStation 5, Digital Edition, HDMI - USB, SSD da 825 GB, Controller wireless DualSense',399.99,1);