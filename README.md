# Assignment1 - DevOps

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/ggmd/2020_assignment1_devops/master?label=prod)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/ggmd/2020_assignment1_devops/develop?label=dev)
![npm](https://img.shields.io/npm/v/npm)
![node](https://img.shields.io/badge/node-v10.19.0-green)
![eslint](https://img.shields.io/badge/eslint-%5E7.11.0-red)

Gitlab: https://gitlab.com/ggmd/2020_assignment1_devops

Web API: https://assignment1-devops.herokuapp.com/api/v1/products

## Table of Contents
- [Group Components](#group-components)
- [Description](#description)
    - [API](#api)
    - [DB](#db)
- [Gitlab-CI Pipeline](#gitlab-ci-pipeline)
- [Requirements](#requirements)
- [Development Setup](#development-setup)
- [Production Setup](#production-setup)
- [Testing](#testing)
- [Flow of Work](#flow-of-work)

## Group Components
- Davide Pietrasanta; 844824
- Gaetano Magazzù; 829685
- Giuseppe Magazzù; 829612
- Mirco Malanchini; 829889

## Description
Our goal is to set up a CI/CD pipeline to automate the entire development process using the Gitlab CI/CD infrastructure.
We've implemented a simple web API application aimed to simulate an inventory of products.

### API
The following endpoints have been implemented:
- GET /api/v1/products/
- GET /api/v1/products/:id
- POST /api/v1/products/
- DELETE /api/v1/products/:id

### DB
A postgres database is used by gitlab services for the integration tests.

For deploy is used a postgres database on heroku.

## Gitlab-CI Pipeline
#### build
Install dependencies using npm, only if there are changes in the package-lock.json file. The dependencies are cached and shared between jobs.
#### verify
Runs static analysis using eslint. Failure is allowed.
#### tests
1. the unit tests are executed using jest
2. the integration tests are executed using jest for normal tests and supertest for the web api. In this job is used a postgres database from gitlab services.
#### package
Application is bundled and passed to the next stage as an artifact
#### release
Create a docker container containing the application (bundle.js) and the package.json. The docker image is retrived from the cache, if present, and pushed to the heroku registry.
#### deploy
The docker image is retrived from the heroku registry and pushed to  the heroku application. Heroku starts automatically the deploy. 

## Requirements
This project is currently supported and tested in the following environment:
- [node.js][node] v10.19.0
- [npm][npm] v6.14.8

[node]: https://nodejs.org/
[npm]: https://www.npmjs.com/

## Development Setup
```
npm install
npm start
```

## Production Setup
```
npm install
npm run build
npm run prod
```

## Testing
```
npm test
```

## Flow of Work
There are 3 branches:

**develop**

We started with the implementation of the "develop" branch, which was a simple implementation of the project without any database.
Here we implemented the unit tests (in [v1.0.1](https://gitlab.com/ggmd/2020_assignment1_devops/-/tree/v1.0.1)) and than merge this branch with "master".

**database**

After the merge we started with the implementation of the "database" branch, which was a simple implementation of the project using a database. Here we implemented the integration tests and than merge this branch with "master".

**master**

In the final "master" branch there is a simple implementation of the project using a database.
There is no unit tests in this branch because we left it in the "develop" branch. 
