const { body, param } = require('express-validator')
const db = require('../database/database')

// Retrieve all products
exports.findAll = (req, res, next) => {
  db.products.findAll()
    .then(products => res.status(200).json(products))
    .catch(err => next(err))
}

// Find a single product with a id
exports.findById = (req, res, next) => {
  const id = parseInt(req.params.id)

  db.products.findByPk(id)
    .then(product => {
      if (!product) {
        res.status(404).json({ status: 404, error: 'no products found' })
      } else {
        res.status(200).json(product)
      }
    })
    .catch(err => next(err))
}

// Insert a product
exports.insert = (req, res, next) => {
  const id = parseInt(req.body.id)
  const name = req.body.name
  const description = req.body.description
  const quantity = parseInt(req.body.quantity)
  const price = parseFloat(req.body.price)

  const product = { id, name, description, quantity, price }

  db.products.create(product)
    .then(product => res.status(201).json(product))
    .catch(err => next(err))
}

// Delete a product
exports.delete = (req, res, next) => {
  const id = parseInt(req.params.id)

  db.products.findByPk(id)
    .then(product => {
      if (!product) {
        return res.status(404).json({
          status: 404,
          error: 'no products found'
        })
      }

      product.destroy()
        .then(deleted => {
          if (deleted === 1) {
            return res.status(204).send()
          }

          return res.status(410).json({
            status: 410,
            error: 'the product could not be deleted'
          })
        })
        .catch(err => next(err))
    })
    .catch(err => next(err))
}

// Validate method
exports.validate = (method) => {
  switch (method) {
    case 'findAll':
      return []
    case 'insert':
      return [
        body('id', 'field is required').exists({ checkNull: true }),
        body('id', 'field must be a number greater than zero').isInt({ gt: 0 }),
        body('name', 'field is required').notEmpty(),
        body('name', 'field must be a string').isString(),
        body('description', 'field is required').notEmpty(),
        body('description', 'field must be a string').isString(),
        body('quantity', 'field is required').exists({ checkNull: true }),
        body('quantity', 'field must be a number greater than zero').isInt({ gt: 0 }),
        body('price', 'field is required').exists({ checkNull: true }),
        body('price', 'field must be a decimal greater than zero').isFloat({ gt: 0 })
      ]
    case 'delete':
    case 'findById':
      return [
        param('id', 'field is required').exists({ checkNull: true }),
        param('id', 'field must be a number greater than zero').isInt({ gt: 0 })
      ]
  }
}
