module.exports = (err, req, res, next) => {
  if (req.app.get('env') !== 'production') {
    console.error(err)
  }

  return res.status(500).json({
    status: 500,
    error: err.message || 'some error occurred'
  })
}
