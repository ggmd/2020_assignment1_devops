const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const db = require('./database/database')
const app = express()

app.use(bodyParser.json())
app.use(cors())

const productRoutes = require('./routes/product')
const notFound = require('./middlewares/notFound')
const errors = require('./middlewares/errors')

app.use('/api/v1/products', productRoutes)
app.use(errors)
app.use(notFound)

const port = process.env.PORT || 8080

db.sequelize.sync()
  .then(() => app.listen(port, '0.0.0.0'))
  .catch(err => console.log(err))

module.exports = app
