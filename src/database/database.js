const config = require('./config')
const Sequelize = require('sequelize')

const sequelize = new Sequelize(config[process.env.NODE_ENV])

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.products = require('../models/Product')(sequelize, Sequelize)

module.exports = db
