FROM node:12

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY dist/bundle.js .
COPY package*.json ./

RUN npm install --production

EXPOSE 3000

CMD ["node","bundle.js"]
