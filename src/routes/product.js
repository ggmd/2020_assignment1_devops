const express = require('express')
const router = express.Router()

const controller = require('../controllers/product')
const validation = require('../middlewares/validation')

// GET /v1/api/products/
router.get(
  '/',
  controller.validate('findAll'),
  validation,
  controller.findAll
)

// GET /v1/api/products/:id
router.get(
  '/:id',
  controller.validate('findById'),
  validation,
  controller.findById
)

// POST /v1/api/products/
router.post(
  '/',
  controller.validate('insert'),
  validation,
  controller.insert
)

// DELETE /v1/api/products/:id
router.delete(
  '/:id',
  controller.validate('delete'),
  validation,
  controller.delete
)

module.exports = router
